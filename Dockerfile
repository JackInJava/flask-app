FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pop install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", " ./run.py"]